 var firebaseConfig = {
    apiKey: "AIzaSyBN1HhAtT-7Xi_jgJh5bDzgzeYTW6sSdqI",
    authDomain: "vue-chat-8b180.firebaseapp.com",
    databaseURL: "https://vue-chat-8b180.firebaseio.com",
    projectId: "vue-chat-8b180",
    storageBucket: "vue-chat-8b180.appspot.com",
    messagingSenderId: "1030244067808",
    appId: "1:1030244067808:web:fcf3af9c1c2d976f"
  };
  // Initialize Firebase
 const firebaseApp =  firebase.initializeApp(firebaseConfig);
 firebaseApp.firestore().settings({
     timestampsInSnapshots: true
 });

 export default firebase.firestore();
